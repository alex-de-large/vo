extern crate nalgebra as na;
extern crate vo_core;
extern crate kiss3d;
extern crate adjacent_pair_iterator;

pub mod renderer;
pub mod visualizer;
mod vo_camera;
mod conversion;

use kiss3d::renderer::Renderer;
use kiss3d::camera::Camera;
use kiss3d::planar_camera::PlanarCamera;
use kiss3d::post_processing::PostProcessingEffect;
use kiss3d::text::Font;
use kiss3d::window::{State, Window};

use na::{Point2, Point3, Vector3, Translation3, UnitQuaternion};

use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

use crate::renderer::PointCloudRenderer;
use crate::visualizer::{Visualizer, VisualizerData};

use vo_core::point_cloud::PointCloud;

use vo_camera::VoCamera;

use adjacent_pair_iterator::AdjacentPairIterator;
use std::cell::RefCell;
use std::rc::Rc;
use kiss3d::resource::Mesh;
use kiss3d::scene::SceneNode;


pub struct VoVisualizerData {
    pub point_cloud: PointCloud,
    pub pos: Point3<f32>,
    pub rot: UnitQuaternion<f32>
}

impl VisualizerData for VoVisualizerData {

}

impl VoVisualizerData {

    pub fn new(point_cloud: PointCloud, pos: Point3<f32>, rot: UnitQuaternion<f32>) -> Self {
        Self {
            point_cloud,
            pos,
            rot
        }
    }
}

// Writing a custom renderer requires the main loop to be handled by the `State`
pub struct VoVisualizerState {
    point_cloud_renderer: PointCloudRenderer,
    receiver: Receiver<VoVisualizerData>,
    path: Vec<Point3<f32>>,
    // vo_camera_mesh: SceneNode,
    trajectory_color: Point3<f32>
}

impl VoVisualizerState {

    pub fn new(point_cloud_render: PointCloudRenderer,
               receiver: Receiver<VoVisualizerData>)
        -> Self
    {
        Self {
            point_cloud_renderer: point_cloud_render,
            receiver,
            path: vec![],
            // vo_camera_mesh,
            trajectory_color: Point3::new(1.0, 0.0, 1.0)
        }
    }
}

impl State for VoVisualizerState {

    fn step(&mut self, window: &mut Window) {

        let data = self.receiver.try_recv();
        if !data.is_err() {
            let mut unwrapped = data.unwrap();
            self.point_cloud_renderer.append(unwrapped.point_cloud.data.as_mut());

            self.path.push(unwrapped.pos.xyz());

            // self.vo_camera_mesh.set_local_translation(Translation3::from(unwrapped.pos));
            // self.vo_camera_mesh.set_local_rotation(unwrapped.rot);
        }

        // draw points number
        let num_points_text = format!(
            "Points number: {}",
            self.point_cloud_renderer.points_num()
        );
        window.draw_text(
            &num_points_text,
            &Point2::new(0.0, 20.0),
            40.0,
            &Font::default(),
            &Point3::new(1.0, 1.0, 1.0),
        );

        // draw coordinate axis
        let origin = &Point3::new(0.0, 0.0, 0.0);
        // x
        window.draw_line(
            origin,
            &Point3::new(1.0, 0.0, 0.0),
            &Point3::new(1.0, 0.0, 0.0)
        );
        // y
        window.draw_line(
            origin,
            &Point3::new(0.0, 1.0, 0.0),
            &Point3::new(0.0, 1.0, 0.0)
        );
        // z
        window.draw_line(
            origin,
            &Point3::new(0.0, 0.0, 1.0),
            &Point3::new(0.0, 0.0, 1.0)
        );

        // draw path
        for pair in self.path.iter().skip(1).adjacent_pairs() {
            window.draw_line(pair.0, pair.1, &self.trajectory_color);
        }
    }

    // Return the custom renderer that will be called at each render loop.
    fn cameras_and_effect_and_renderer(&mut self) -> (
        Option<&mut dyn Camera>,
        Option<&mut dyn PlanarCamera>,
        Option<&mut dyn Renderer>,
        Option<&mut dyn PostProcessingEffect>,
    ) {
        (None, None, Some(&mut self.point_cloud_renderer), None)
    }
}


pub struct VoVisualizer {
    width: u32,
    height: u32,
    background_color: Point3<f32>,
}

impl Visualizer<VoVisualizerData> for VoVisualizer {

    fn start(&mut self, initial_data: VoVisualizerData, receiver: Receiver<VoVisualizerData>) {

        let mut window = Window::new_with_size("Visualizer", self.width, self.height);
        window.set_background_color(
            self.background_color[0],
            self.background_color[1],
            self.background_color[2]
        );

        let point_cloud_renderer = PointCloudRenderer::new(
            3.0,
            initial_data.point_cloud
        );
        // let mesh = Rc::new(RefCell::new(Mesh::new(
        //     VoCamera::get_mesh_points(&initial_data.pos),
        //     VoCamera::get_mesh_indices(),
        //     None,
        //     None,
        //     false
        // )));
        // let mut c = window.add_mesh(mesh, Vector3::new(1.0, 1.0, 1.0));
        // c.set_color(1.0, 0.0, 0.0);
        // c.set_lines_width(3.0);
        // c.set_surface_rendering_activation(false);

        let mut state = VoVisualizerState::new(
            point_cloud_renderer,
            receiver
        );
        state.path.push(initial_data.pos);

        window.render_loop(state);
    }
}

impl VoVisualizer {

    pub fn new(width: u32, height: u32, background_color: Point3<f32>) -> Self {
        Self {
            width,
            height,
            background_color
        }
    }
}
