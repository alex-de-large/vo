use na::{Point3, UnitQuaternion, Quaternion};
use kiss3d::nalgebra::Vector4;

pub fn convert_point(point: &Point3<f32>) -> Point3<f32> {
    point.xyz()
}

pub fn convert_quaternion(quaternion: &UnitQuaternion<f32>) -> UnitQuaternion<f32> {
    let c = quaternion.coords;
    UnitQuaternion::from_quaternion(Quaternion {coords: Vector4::new(c[1], c[2], c[0], c[3])})
}