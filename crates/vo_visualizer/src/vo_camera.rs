use na::{Point3, Vector3, Isometry3};
use kiss3d::scene::Object;

pub struct VoCamera;

impl VoCamera {

    pub fn get_mesh_points(pos: &Point3<f32>) -> Vec<Point3<f32>> {
        vec![
            pos.xyz(),
            pos + Vector3::new(0.1, -0.1, 0.1),
            pos + Vector3::new(-0.1, -0.1, 0.1),
            pos + Vector3::new(-0.1, -0.1, -0.1),
            pos + Vector3::new(0.1, -0.1, -0.1)
        ]
    }

    pub fn get_mesh_indices() -> Vec<Point3<u16>> {
        vec![
            Point3::new(0u16, 1, 2),
            Point3::new(0u16, 2, 3),
            Point3::new(0u16, 3, 4),
            Point3::new(0u16, 1, 4),
        ]
    }
}
