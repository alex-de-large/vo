use na::{Matrix4, Point3};
use kiss3d::camera::Camera;
use kiss3d::context::Context;
use kiss3d::renderer::Renderer;
use kiss3d::resource::{
    AllocationType, BufferType, Effect, GPUVec, ShaderAttribute, ShaderUniform,
};

use vo_core::point_cloud::PointCloud;

use crate::conversion;


pub type PointCloudGPU = GPUVec<Point3<f32>>;


// Structure which manages the display of long-living points.
pub struct PointCloudRenderer {
    shader: Effect,
    pos: ShaderAttribute<Point3<f32>>,
    color: ShaderAttribute<Point3<f32>>,
    proj: ShaderUniform<Matrix4<f32>>,
    view: ShaderUniform<Matrix4<f32>>,
    point_cloud_gpu: PointCloudGPU,
    point_size: f32,
}

impl PointCloudRenderer {

    pub fn new(point_size: f32, point_cloud: PointCloud) -> Self {
        let mut shader = Effect::new_from_str(VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC);
        shader.use_program();
        Self {
            point_cloud_gpu: GPUVec::new(point_cloud.data,  BufferType::Array, AllocationType::StreamDraw),
            pos: shader.get_attrib::<Point3<f32>>("position").unwrap(),
            color: shader.get_attrib::<Point3<f32>>("color").unwrap(),
            proj: shader.get_uniform::<Matrix4<f32>>("proj").unwrap(),
            view: shader.get_uniform::<Matrix4<f32>>("view").unwrap(),
            shader,
            point_size,
        }
    }

    pub fn push(&mut self, point: Point3<f32>, color: Point3<f32>) {
        if let Some(points) = self.point_cloud_gpu.data_mut() {
            points.push(point);
            points.push(color);
        }
    }

    pub fn append(&mut self, points: &mut Vec<Point3<f32>>) {
        if let Some(points_gpu) = self.point_cloud_gpu.data_mut() {
            // points_gpu.append(&mut points.iter_mut().map(|p| {conversion::convert_point(p) }).collect::<Vec<_>>());
            points_gpu.append(points);
        }
    }

    pub fn points_num(&self) -> usize {
        self.point_cloud_gpu.len() / 2
    }
}

impl Renderer for PointCloudRenderer {

    fn render(&mut self, pass: usize, camera: &mut dyn Camera) {
        if self.point_cloud_gpu.len() == 0 {
            return;
        }

        self.shader.use_program();
        self.pos.enable();
        self.color.enable();

        camera.upload(pass, &mut self.proj, &mut self.view);

        self.pos.bind_sub_buffer(&mut self.point_cloud_gpu, 1, 0);
        self.color.bind_sub_buffer(&mut self.point_cloud_gpu, 1, 1);

        let context = Context::get();
        context.point_size(self.point_size);
        context.draw_arrays(Context::POINTS, 0, (self.point_cloud_gpu.len() / 2) as i32);

        self.pos.disable();
        self.color.disable();
    }
}

const VERTEX_SHADER_SRC: &'static str = "#version 100
    attribute vec3 position;
    attribute vec3 color;
    varying   vec3 Color;
    uniform   mat4 proj;
    uniform   mat4 view;
    void main() {
        gl_Position = proj * view * vec4(position, 1.0);
        Color = color;
    }";

const FRAGMENT_SHADER_SRC: &'static str = "#version 100
#ifdef GL_FRAGMENT_PRECISION_HIGH
   precision highp float;
#else
   precision mediump float;
#endif
    varying vec3 Color;
    void main() {
        gl_FragColor = vec4(Color, 1.0);
    }";
