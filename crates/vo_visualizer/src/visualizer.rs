use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

pub trait Visualizer<T: VisualizerData> {

    fn start(&mut self, initial_data: T, receiver: Receiver<T>);

    fn get_data_channels() -> (Sender<T>, Receiver<T>) {
        mpsc::channel::<T>()
    }
}

pub trait VisualizerData {

}

