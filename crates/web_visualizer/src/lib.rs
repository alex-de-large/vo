extern crate image;
extern crate nalgebra;
extern crate console_error_panic_hook;
extern crate vo_visualizer;
extern crate kiss3d;
extern crate adjacent_pair_iterator;

use std::panic;

use vo_core;
use wasm_bindgen::prelude::*;

use nalgebra::DMatrix;
use std::{error::Error, io::Read, io::Seek, io::SeekFrom};

use byteorder::{BigEndian, ReadBytesExt};
use png::HasParameters;
use std::collections::HashMap;
use std::io::Cursor;
use tar;

use vo_core::camera::Intrinsics;
use vo_core::tracker::inverse_compositional as track;
use vo_core::io::tum_rgbd;
use vo_core::conversion;
use vo_core::types::{Point2, Point3, Iso3};
use vo_core::point_cloud::PointCloud;

use kiss3d::camera::Camera;
use kiss3d::planar_camera::PlanarCamera;
use kiss3d::post_processing::PostProcessingEffect;
use kiss3d::renderer::Renderer;
use kiss3d::text::Font;
use kiss3d::window::{State, Window};

use vo_visualizer::renderer::PointCloudRenderer;

use image::{DynamicImage, RgbImage, Rgb};

use adjacent_pair_iterator::AdjacentPairIterator;

// wasm-pack build --target web
// http-server


struct AppState {
    vo: Vo,
    next_frame_index: usize,
    trajectory: Vec<Point3>,
    trajectory_color: Point3,
    point_cloud_renderer: PointCloudRenderer,
}

impl AppState {

    fn new(vo: Vo) -> Self {
        let point_cloud_renderer = PointCloudRenderer::new(1.0, PointCloud::new());
        AppState {
            vo,
            next_frame_index: 1,
            trajectory: vec![],
            trajectory_color: Point3::new(1.0, 0.0, 1.0),
            point_cloud_renderer,
        }
    }
}

impl State for AppState {

    fn step(&mut self, window: &mut Window) {

        // draw points num
        let num_points_text = format!(
            "Number of points: {}",
            self.point_cloud_renderer.points_num()
        );
        window.draw_text(
            &num_points_text,
            &Point2::new(0.0, 20.0),
            60.0,
            &Font::default(),
            &Point3::new(1.0, 1.0, 1.0),
        );

        // draw coordinate axis
        let origin = &Point3::new(0.0, 0.0, 0.0);
        // x
        window.draw_line(
            origin,
            &Point3::new(1.0, 0.0, 0.0),
            &Point3::new(1.0, 0.0, 0.0)
        );
        // y
        window.draw_line(
            origin,
            &Point3::new(0.0, 1.0, 0.0),
            &Point3::new(0.0, 1.0, 0.0)
        );
        // z
        window.draw_line(
            origin,
            &Point3::new(0.0, 0.0, 1.0),
            &Point3::new(0.0, 0.0, 1.0)
        );

        // draw path
        for pair in self.trajectory.iter().adjacent_pairs() {
            window.draw_line(pair.0, pair.1, &self.trajectory_color);
        }

        if self.next_frame_index < self.vo.associations_len() {

            // Track the rgb-d image.
            let keyframe_changed = self.vo.track(self.next_frame_index);

            let (timestamp, pose) = self.vo.tracker.as_ref().unwrap().current_frame();
            // println!("{}", (tum_rgbd::Frame { timestamp, pose }).to_string());
            self.trajectory.push(Point3::from(pose.translation.vector));

            // Render 3d points.
            if keyframe_changed {
                let points_2d = self.vo.tracker.as_ref().unwrap().points_2d();
                let mut colors = extract_colors(&self.vo.last_image.as_ref().unwrap().to_rgb8(), &points_2d);
                for (p, c) in self.vo.tracker.as_ref().unwrap().points_3d().drain(..).zip(colors.drain(..)) {
                    self.point_cloud_renderer.push(p, c);
                }
            }
            self.next_frame_index += 1;
        }
    }

    fn cameras_and_effect_and_renderer(
        &mut self,
    ) -> (
        Option<&mut dyn Camera>,
        Option<&mut dyn PlanarCamera>,
        Option<&mut dyn Renderer>,
        Option<&mut dyn PostProcessingEffect>,
    ) {
        (None, None, Some(&mut self.point_cloud_renderer), None)
    }
}

#[wasm_bindgen]
pub fn start_visualization(vo: Vo) {
    let mut window = Window::new_with_size("Visualizer", 800, 600);
    let app_state = AppState::new(vo);
    window.set_background_color(
        0.0,
        0.0,
        0.0
    );
    window.render_loop(app_state);
}

fn extract_colors(img: &RgbImage, points: &Vec<(usize, usize)>) -> Vec<Point3> {
    let mut res = vec![];
    for (x, y) in points {
        res.push(convert_color(img.get_pixel(*x as u32, *y as u32)))
    }
    res
}

fn convert_color(color: &Rgb<u8>) -> Point3 {
    let data = color.0;
    Point3::new(
        data[0] as f32 / 255.0,
        data[1] as f32 / 255.0,
        data[2] as f32 / 255.0
    )
}


#[wasm_bindgen]
pub struct Vo {
    tar_buffer: Vec<u8>,
    entries: HashMap<String, FileEntry>,
    associations: Vec<tum_rgbd::Association>,
    tracker: Option<track::Tracker>,
    last_image: Option<DynamicImage>
}

#[wasm_bindgen]
impl Vo {

    pub fn new() -> Vo {
        panic::set_hook(Box::new(console_error_panic_hook::hook));

        Vo {
            tar_buffer: Vec::new(),
            entries: HashMap::new(),
            associations: Vec::new(),
            tracker: None,
            last_image: None
        }
    }

    pub fn associations_len(&self) -> usize {
        self.associations.len()
    }

    pub fn allocate(&mut self, length: usize) {
        self.tar_buffer = vec![0; length];
    }

    pub fn memory_pos(&self) -> *const u8 {
        self.tar_buffer.as_ptr()
    }

    pub fn build_entries_map(&mut self) {
        // Init archive from in memory tar buffer.
        let mut archive = tar::Archive::new(self.tar_buffer.as_slice());

        for file in archive.entries().expect("48") {
            // Check for an I/O error.
            let file = file.expect("50");
            self.entries.insert(
                file.path().unwrap().to_str().expect("52").to_owned(),
                FileEntry {
                    offset: file.raw_file_position(),
                    length: file.header().size().expect("55"),
                },
            );
        }
    }

    pub fn init(&mut self, camera_id: &str, levels: i32) {
        let intrinsics = create_camera(camera_id).expect("Unknown camera id.");
        let mut buffer_cursor = Cursor::new(&self.tar_buffer);
        let associations_buffer =
            get_buffer("associations.txt", &mut buffer_cursor, &self.entries).expect("Can`t find associations.txt in tar.");
        self.associations = parse_associations_buf(associations_buffer.as_slice()).expect("64");

        // Setup tracking configuration.
        let config = track::Config {
            nb_levels: levels as usize,
            candidates_diff_threshold: 7,
            depth_scale: tum_rgbd::DEPTH_SCALE,
            intrinsics: intrinsics,
            idepth_variance: 0.0001,
        };

        // Initialize tracker with first depth and color image.
        let (depth_map, img) =
            read_images(&self.associations[0], &mut buffer_cursor, &self.entries).expect("81");
        let depth_time = self.associations[0].depth_timestamp;
        let img_time = self.associations[0].color_timestamp;
        self.tracker = Some(config.init(depth_time, &depth_map, img_time, conversion::matrix_from_image(img.to_luma8())));
    }

    pub fn track(&mut self, frame_id: usize) -> bool {
        let mut buffer_cursor = Cursor::new(&self.tar_buffer);
        let assoc = &self.associations[frame_id];
        let (depth_map, img) = read_images(assoc, &mut buffer_cursor, &self.entries).expect("92");

        // Track the rgb-d image.
        if let Some(ref mut t) = self.tracker {
            let keyframe_changed = t.track(
                false,
                assoc.depth_timestamp,
                &depth_map,
                assoc.color_timestamp,
                conversion::matrix_from_image(img.to_luma8())
            );
            self.last_image = Some(img);
            return keyframe_changed;
        };

        // Return formatted camera pose.
        unreachable!()
    }

    pub fn points(&self) {

    }
}

/// Create camera depending on `camera_id` command line argument.
fn create_camera(camera_id: &str) -> Result<Intrinsics, String> {
    match camera_id {
        "fr1" => Ok(tum_rgbd::INTRINSICS_FR1),
        "fr2" => Ok(tum_rgbd::INTRINSICS_FR2),
        "fr3" => Ok(tum_rgbd::INTRINSICS_FR3),
        "icl" => Ok(tum_rgbd::INTRINSICS_ICL_NUIM),
        _ => {
            Err(format!("Unknown camera id: {}", camera_id))
        }
    }
}

/// Open an association file (in bytes form) and parse it into a vector of Association.
fn parse_associations_buf(buffer: &[u8]) -> Result<Vec<tum_rgbd::Association>, Box<dyn Error>> {
    let mut content = String::new();
    let mut slice = buffer;
    slice.read_to_string(&mut content)?;
    tum_rgbd::parse::associations(&content).map_err(|s| s.into())
}

struct FileEntry {
    offset: u64,
    length: u64,
}

fn get_buffer<R: Read + Seek>(
    name: &str,
    file: &mut R,
    entries: &HashMap<String, FileEntry>,
) -> Result<Vec<u8>, std::io::Error> {
    let entry = entries.get(name).expect("Entry is not in archive");
    read_file_entry(entry, file)
}

fn read_file_entry<R: Read + Seek>(
    entry: &FileEntry,
    file: &mut R,
) -> Result<Vec<u8>, std::io::Error> {
    let mut buffer = vec![0; entry.length as usize];
    file.seek(SeekFrom::Start(entry.offset))?;
    file.read_exact(&mut buffer)?;
    Ok(buffer)
}

/// Read a depth and color image given by an association.
fn read_images<R: Read + Seek>(
    assoc: &tum_rgbd::Association,
    file: &mut R,
    entries: &HashMap<String, FileEntry>,
) -> Result<(DMatrix<u16>, DynamicImage), image::ImageError> {
    // Read depth image.
    let depth_path_str = assoc.depth_file_path.to_str().expect("err").to_owned();
    let depth_buffer = get_buffer(&depth_path_str, file, entries)?;
    let (w, h, depth_map_vec_u16) = read_png_16bits_buf(depth_buffer.as_slice()).unwrap();
    let depth_map = DMatrix::from_row_slice(h, w, depth_map_vec_u16.as_slice());

    // Read color image.
    let img_path_str = assoc.color_file_path.to_str().expect("err").to_owned();
    let img_buffer = get_buffer(&img_path_str, file, entries)?;
    let img = image::load(Cursor::new(img_buffer), image::ImageFormat::Png)?;

    Ok((depth_map, img))
}

fn read_png_16bits_buf<R: Read>(r: R) -> Result<(usize, usize, Vec<u16>), png::DecodingError> {
    let mut decoder = png::Decoder::new(r);
    decoder.set(png::Transformations::IDENTITY);
    let (info, mut reader) = decoder.read_info()?;
    let mut buffer = vec![0; info.buffer_size()];
    reader.next_frame(&mut buffer)?;

    let mut buffer_u16 = vec![0; (info.width * info.height) as usize];
    let mut buffer_cursor = Cursor::new(buffer);
    buffer_cursor.read_u16_into::<BigEndian>(&mut buffer_u16)?;

    Ok((info.width as usize, info.height as usize, buffer_u16))
}