import { Vo, default as init, start_visualization } from "./pkg/web_visualizer.js";

const file_input = document.getElementById("file-input");
const levels_input = document.getElementById("levels");
const file_reader = new FileReader();
file_input.addEventListener("change", () => loadInput(file_input));

async function run() {
  // Initialize the wasm module.
  const wasm = await init("./pkg/web_visualizer_bg.wasm");
  const vo = Vo.new();

  // Transfer archive data to wasm when the file is loaded.
  file_reader.onload = () =>
    transferContent(file_reader.result, vo, wasm);
}

function loadInput(input) {
  const tar_file = input.files[0];
  file_reader.readAsArrayBuffer(tar_file);
}

// Transfer archive data to wasm when the file is loaded.
function transferContent(arrayBuffer, vo, wasm) {
  vo.allocate(arrayBuffer.byteLength);
  const wasm_buffer = new Uint8Array(wasm.memory.buffer);
  const start = vo.memory_pos();
  const file_buffer = new Uint8Array(arrayBuffer);
  wasm_buffer.set(file_buffer, start);
  console.log("Building entries hash map ...");
  vo.build_entries_map();
  console.log("Initializing tracker with first image ...");
  vo.init("icl", parseInt(levels_input.value));
  console.log("Starting animation frame loop ...");
  start_visualization(vo);
}


run();