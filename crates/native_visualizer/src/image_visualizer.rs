use vo_visualizer::visualizer::{Visualizer, VisualizerData};

use image::DynamicImage;
use std::sync::mpsc::Receiver;
use show_image::create_window;
use std::thread;
use std::time::Duration;


pub struct ImageVisualizerData {
    img_name: String,
    img: DynamicImage
}

impl VisualizerData for ImageVisualizerData {

}

impl ImageVisualizerData {

    pub fn new(img_name: String, img: DynamicImage) -> Self {
        Self { img_name, img }
    }
}


pub struct ImageVisualizer {

}

impl ImageVisualizer {

    pub fn new() -> Self {
        Self {}
    }
}

impl Visualizer<ImageVisualizerData> for ImageVisualizer {

    fn start(&mut self, initial_data: ImageVisualizerData, receiver: Receiver<ImageVisualizerData>) {
        show_image::run_context(move || {
            let window = create_window("image", Default::default()).expect("qwe");
            window.set_image("image", initial_data.img).expect("qwe");
            // 60 fps loop
            loop {
                thread::sleep(Duration::from_millis(15));
                let data = receiver.try_recv();
                if !data.is_err() {
                    let data = data.unwrap();
                    window.set_image(data.img_name, data.img).expect("Set image error");
                }
            }
        });
    }
}