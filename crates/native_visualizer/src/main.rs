mod image_visualizer;

extern crate nalgebra as na;
extern crate image;
extern crate vo_core;

use na::{DMatrix, UnitQuaternion};

use std::{env, error::Error, fs, io::BufReader, io::Read, path::Path, path::PathBuf, thread};
use std::time::{Instant, Duration};
use std::fs::File;
use std::io::Write;

use vo_core::camera::Intrinsics;
use vo_core::tracker::inverse_compositional;
use vo_core::io::tum_rgbd;
use vo_core::{tools, conversion};
use vo_core::types::{Point3};
use vo_core::io::tum_rgbd::Association;
use vo_core::point_cloud::PointCloud;
use vo_core::eval;

use image::{DynamicImage, RgbImage, Rgb, Pixel};

use image_visualizer::{ImageVisualizer, ImageVisualizerData};
use vo_visualizer::visualizer::Visualizer;
use vo_visualizer::{VoVisualizer, VoVisualizerData};


const LIM: usize = 2000;
const USAGE: &str = "Args: [fr1|fr2|fr3|icl] associations_file";


pub fn test_eval() {
    let traj = tum_rgbd::parse::trajectory(
        fs::read_to_string("traj.txt")
            .unwrap().as_str()
    ).unwrap();
    let gt = tum_rgbd::parse::trajectory(
        fs::read_to_string("gt.txt")
            .unwrap().as_str()
    ).unwrap();
    println!("{:?}", eval::rmse(&traj, &gt));
}


pub fn main() {
    // let args: Vec<String> = env::args().collect();
    // if let Err(error) = operate_on(&args) {
    //     eprintln!("{:?}", error);
    // }
    test_eval();
}

pub fn operate_on(args: &[String]) -> Result<(), Box<dyn Error>> {

    let valid_args = parse_args(args)?;
    let associations = parse_associations(valid_args.associations_file_path)?;

    let config = inverse_compositional::Config {
        nb_levels: 6,
        candidates_diff_threshold: 7,
        depth_scale: tum_rgbd::DEPTH_SCALE,
        intrinsics: valid_args.intrinsics,
        idepth_variance: 0.0001,
    };

    // Initialize tracker with first depth and color image.
    let (depth_map, img) = read_image(&associations[0])?;
    let depth_time = associations[0].depth_timestamp;
    let img_time = associations[0].color_timestamp;
    let mut tracker = config.init(
        depth_time,
        &depth_map,
        img_time,
        conversion::matrix_from_image(img.to_luma8())
    );

    // Initialize visualizers
    let (image_sender, image_receiver) = ImageVisualizer::get_data_channels();
    let (vo_sender, vo_receiver) = VoVisualizer::get_data_channels();

    let mut image_visualizer = ImageVisualizer::new();
    let mut vo_visualizer = VoVisualizer::new(800, 600, Point3::new(0.0, 0.0, 0.0));

    let initial_image_data = ImageVisualizerData::new(String::from("img"), img);
    let initial_vo_data = VoVisualizerData::new(
        PointCloud::new(),
        Point3::new(0.0, 0.0, 0.0),
        UnitQuaternion::from_euler_angles(0.0, 0.0, 0.0)
    );
    let result_file_path = "traj.txt";
    let mut result_file = File::create(result_file_path).unwrap();
    let _vo_thread = thread::spawn(move || {
        // wait for visualizers to initialize
        thread::sleep(Duration::from_millis(1500));
        for (i, assoc) in associations.iter().enumerate().skip(1) {
            if i > LIM { break; }
            let (depth_map, img) = read_image(assoc).unwrap();

            let keyframe_changed = tracker.track(
                false,
                assoc.depth_timestamp,
                &depth_map,
                assoc.color_timestamp,
                conversion::matrix_from_image(img.to_luma8())
            );

            let (timestamp, pose) = tracker.current_frame();

            let frame = tum_rgbd::Frame {timestamp, pose};
            println!("{}", frame.to_string());
            result_file.write(frame.to_string().as_bytes());
            result_file.write("\n".as_bytes());

            if keyframe_changed {
                let mut point_cloud = PointCloud::new();
                let mut points = tracker.points_3d();
                let mut colors = extract_colors(&img.to_rgb8(), &tracker.points_2d());
                point_cloud.append(&mut points, &mut colors);
                drop(vo_sender.send(VoVisualizerData::new(
                    point_cloud,
                    Point3::from(pose.translation.vector),
                    UnitQuaternion::from(pose.rotation)
                )));
            }
            drop(image_sender.send(ImageVisualizerData::new(
                "img".to_string(),
                img
            )));
            thread::sleep(Duration::from_millis(15));
        }
    });

    let _vo_visualizer_thread = thread::spawn(move || {
        vo_visualizer.start(initial_vo_data, vo_receiver);
    });

    image_visualizer.start(initial_image_data, image_receiver);

    Ok(())
}

fn extract_colors(img: &RgbImage, points: &Vec<(usize, usize)>) -> Vec<Point3> {
    let mut res = vec![];
    for (x, y) in points {
        res.push(convert_color(img.get_pixel(*x as u32, *y as u32)))
    }
    res
}

fn convert_color(color: &Rgb<u8>) -> Point3 {
    let data = color.0;
    Point3::new(
        data[0] as f32 / 255.0,
        data[1] as f32 / 255.0,
        data[2] as f32 / 255.0
    )
}

fn point_data_to_string(point: &Point3, color: &Rgb<u8>) -> String {
    let c_data = convert_color(color);
    format!("{} {} {} {} {} {}\n", point.x, point.y, point.z, c_data[0], c_data[1], c_data[2])
}

#[derive(Debug)]
struct Args {
    associations_file_path: PathBuf,
    intrinsics: Intrinsics,
    save_data: bool
}

/// Verify that command line arguments are correct.
fn parse_args(args: &[String]) -> Result<Args, String> {
    if let [_, camera_id, associations_file_path_str] = args {
        let intrinsics = create_camera(camera_id)?;
        let associations_file_path = PathBuf::from(associations_file_path_str);
        if associations_file_path.is_file() {
            Ok(Args {
                intrinsics,
                associations_file_path,
                save_data: true
            })
        } else {
            eprintln!("{}", USAGE);
            Err(format!(
                "The association file does not exist or is not reachable: {}",
                associations_file_path_str
            ))
        }
    } else {
        eprintln!("{}", USAGE);
        Err("Wrong number of arguments".to_string())
    }
}

/// Create camera depending on `camera_id` command line argument.
fn create_camera(camera_id: &str) -> Result<Intrinsics, String> {
    match camera_id {
        "fr1" => Ok(tum_rgbd::INTRINSICS_FR1),
        "fr2" => Ok(tum_rgbd::INTRINSICS_FR2),
        "fr3" => Ok(tum_rgbd::INTRINSICS_FR3),
        "icl" => Ok(tum_rgbd::INTRINSICS_ICL_NUIM),
        _ => {
            eprintln!("{}", USAGE);
            Err(format!("Unknown camera id: {}", camera_id))
        }
    }
}

/// Open an association file and parse it into a vector of Association.
fn parse_associations<P: AsRef<Path>>(
    file_path: P,
) -> Result<Vec<tum_rgbd::Association>, Box<dyn Error>> {
    let file = fs::File::open(&file_path)?;
    let mut file_reader = BufReader::new(file);
    let mut content = String::new();
    file_reader.read_to_string(&mut content)?;
    tum_rgbd::parse::associations(&content)
        .map(|v| v.iter().map(|a| abs_path(&file_path, a)).collect())
        .map_err(|s| s.into())
}

/// Transform relative images file paths into absolute ones.
fn abs_path<P: AsRef<Path>>(file_path: P, assoc: &tum_rgbd::Association) -> tum_rgbd::Association {
    let parent = file_path
        .as_ref()
        .parent()
        .expect("How can this have no parent");
    tum_rgbd::Association {
        depth_timestamp: assoc.depth_timestamp,
        depth_file_path: parent.join(&assoc.depth_file_path),
        color_timestamp: assoc.color_timestamp,
        color_file_path: parent.join(&assoc.color_file_path),
    }
}

/// Read a depth and color image given by an association.
fn read_image(
    assoc: &tum_rgbd::Association,
) -> Result<(DMatrix<u16>, DynamicImage), Box<dyn Error>> {
    let (w, h, depth_map_vec_u16) = tools::read_png_16bits(&assoc.depth_file_path)?;
    let depth_map = DMatrix::from_row_slice(h, w, depth_map_vec_u16.as_slice());
    let img = image::open(&assoc.color_file_path)?;
    Ok((depth_map, img))
}
