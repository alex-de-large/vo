use crate::io::tum_rgbd::Frame;
use crate::types::{Float, Iso3};
use crate::math::{so3, se3};

/// calc root-mean-squared error (RMSE) for trajectory and ground truth.
/// Works with icl_nuim dataset format.
pub fn rmse(traj: &Vec<Frame>, gt: &Vec<Frame>) -> Vec<Float> {
    let mut rmse_vec = vec![];
    let mut err_sum = 0.0;

    for (n, data) in traj.iter().zip(gt.iter()).enumerate() {
        let p1 = data.0.pose; let p2 = data.1.pose;
        err_sum += se3::log(p2.inverse() * p1).magnitude();
        rmse_vec.push((err_sum / n as Float).sqrt());
    }

    rmse_vec
}
