use nalgebra::{DMatrix, Scalar};

/// Select a subset of points satisfying two conditions:
///   * points shall be well-distributed in the image.
///   * higher density where gradients are bigger.
/// Last mask contains candidates for the highest resolution
pub fn select<T>(diff_threshold: T, gradients: &[DMatrix<T>]) -> Vec<DMatrix<bool>>
    where
        T: Scalar + std::cmp::PartialOrd + std::ops::Add<Output = T> + Copy,
{
    let (nrows, ncols) = gradients.last().unwrap().shape();
    let mut init_candidates = Vec::new();
    init_candidates.push(create_initial_mask(nrows, ncols));
    let select_function = |a, b, c, d| select_with_threshold(diff_threshold, a, b, c, d);

    gradients
        .iter()
        .rev() // start with lower res
        .skip(1) // skip lower since all points are good
        .fold(init_candidates, |mut multires_masks, grad_mat| {
            let new_mask = calc_next_mask(multires_masks.last().unwrap(), grad_mat, select_function);
            multires_masks.push(new_mask);
            multires_masks
        })
}

/// Discards pixels on sides for the lowest resolution (will propagate).
fn create_initial_mask(nrows: usize, ncols: usize) -> DMatrix<bool> {
    let mut mask = DMatrix::repeat(nrows, ncols, true);
    for pix in mask.column_mut(0).iter_mut() {
        *pix = false;
    }
    for pix in mask.column_mut(ncols - 1).iter_mut() {
        *pix = false;
    }
    for pix in mask.row_mut(0).iter_mut() {
        *pix = false;
    }
    for pix in mask.row_mut(nrows - 1).iter_mut() {
        *pix = false;
    }
    mask
}

fn calc_next_mask<T, F>(prev_mask: &DMatrix<bool>, gradient: &DMatrix<T>, f: F) -> DMatrix<bool>
    where
        T: Scalar + Copy,
        F: Fn(T, T, T, T) -> [bool; 4],
{
    let (nrows, ncols) = gradient.shape();
    let (nrows_2, ncols_2) = prev_mask.shape();
    assert_eq!((nrows_2, ncols_2), (nrows / 2, ncols / 2));
    let mut mask = DMatrix::repeat(nrows, ncols, false);
    for j in 0..(ncols_2) {
        for i in 0..(nrows_2) {
            if prev_mask[(i, j)] {
                let offsets = calc_offsets(i, j);
                let selected = f(
                    gradient[offsets[0]],
                    gradient[offsets[1]],
                    gradient[offsets[2]],
                    gradient[offsets[3]]
                );
                mask[offsets[0]] = selected[0];
                mask[offsets[1]] = selected[1];
                mask[offsets[2]] = selected[2];
                mask[offsets[3]] = selected[3];
            }
        }
    }
    mask
}

fn calc_offsets(i: usize, j: usize) -> [(usize, usize); 4] {
    let m = 2 * i;
    let n = 2 * j;
    [
        (m, n),
        (m + 1, n),
        (m, n + 1),
        (m + 1, n + 1)
    ]
}


/// Discard the 2 or 3 lowest values.
/// The second higher value is kept only if:
///     second > third + thresh
fn select_with_threshold<T>(thresh: T, a: T, b: T, c: T, d: T) -> [bool; 4]
    where
        T: Scalar + std::cmp::PartialOrd + std::ops::Add<Output = T> + Copy,
{
    let mut temp = [(a, 0_usize), (b, 1_usize), (c, 2_usize), (d, 3_usize)];
    temp.sort_unstable_by(|(x, _), (y, _)| x.partial_cmp(y).unwrap());
    let (_, first) = temp[3];
    let (x, second) = temp[2];
    let (y, _) = temp[1];
    let mut result = [false; 4];
    result[first] = true;
    if x > y + thresh {
        result[second] = true;
    }
    result
}
