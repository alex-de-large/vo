pub mod kitty_annotation;
pub mod tum_rgbd;

use std::fs::File;
use std::io::{BufReader, BufRead};

use crate::point_cloud::PointCloud;

use na::{Point3, UnitQuaternion, Quaternion, Vector4};

// format: timestamp [ns], position.x [m], position.y [m], position.z [m], quaternion.x [], quaternion.y [], quaternion.z [], quaternion.w []
pub fn read_trajectory_from_txt(file_path: &str, delimiter: &str) -> anyhow::Result<Vec<(Point3<f32>, UnitQuaternion<f32>)>> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(&file);
    let mut res = vec![];

    for line in reader.lines() {
        let data = line?;
        if data.starts_with("#") {
            continue;
        }
        let data_vec: Vec<f32> = data
            .split(delimiter)
            .map(|word| word.parse::<f32>().unwrap())
            .collect();

        let point = Point3::from_slice(&[data_vec[1], data_vec[2], data_vec[3]]);
        let q: Quaternion<f32> = Quaternion {coords: Vector4::new(data_vec[4], data_vec[5], data_vec[6], data_vec[7])};

        res.push((point, UnitQuaternion::from_quaternion(q)));
    }
    Ok(res)
}

// format: position.x [m], position.y [m], position.z [m], color.r [f32], color.g [f32], color.b [f32]
pub fn read_point_cloud_from_txt(file_path: &str, delimiter: &str) -> anyhow::Result<PointCloud> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(&file);
    let mut pcl_data: PointCloud = PointCloud::new();

    for line in reader.lines() {
        let data = line?;
        if data.starts_with("#") {
            continue;
        }
        let data_point: Vec<f32> = data
            .split(delimiter)
            .map(|word| word.parse::<f32>().unwrap())
            .collect();

        let point = Point3::from_slice(&[data_point[0], data_point[1], data_point[2]]);
        let color =  Point3::from_slice(&[data_point[3], data_point[4], data_point[5]]);

        pcl_data.push(point, color);
    }
    Ok(pcl_data)
}