use nalgebra as na;

/// ( img(i+1,j) - img(i-1,j), img(i,j+1) - img(i,j-1) )
#[allow(clippy::similar_names)]
pub fn centered_double(img: &na::DMatrix<u8>) -> (na::DMatrix<i16>, na::DMatrix<i16>) {
    let (nb_rows, nb_cols) = img.shape();
    let mut grad_x = na::DMatrix::zeros(nb_rows, nb_cols);
    let mut grad_y = na::DMatrix::zeros(nb_rows, nb_cols);
    for j in 1..nb_cols - 2 {
        for i in 1..nb_rows - 2 {
            grad_x[(i, j)] = i16::from(img[(i, j + 1)]) - i16::from(img[(i, j - 1)]);
            grad_y[(i, j)] = i16::from(img[(i + 1, j)]) - i16::from(img[(i - 1, j)]);
        }
    }
    (grad_x, grad_y)
}

/// 1/2 * ( img(i+1,j) - img(i-1,j), img(i,j+1) - img(i,j-1) )
#[allow(clippy::similar_names)]
pub fn centered(img: &na::DMatrix<u8>) -> (na::DMatrix<i16>, na::DMatrix<i16>) {
    let (nb_rows, nb_cols) = img.shape();
    let mut grad_x = na::DMatrix::zeros(nb_rows, nb_cols);
    let mut grad_y = na::DMatrix::zeros(nb_rows, nb_cols);
    for j in 1..nb_cols - 2 {
        for i in 1..nb_rows - 2 {
            grad_x[(i, j)] = (i16::from(img[(i, j + 1)]) - i16::from(img[(i, j - 1)])) / 2;
            grad_y[(i, j)] = (i16::from(img[(i + 1, j)]) - i16::from(img[(i - 1, j)])) / 2;
        }
    }
    (grad_x, grad_y)
}

/// Compute squared gradient norm from x and y gradient matrices.
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::cast_sign_loss)]
pub fn squared_norm(grad_x: &na::DMatrix<i16>, grad_y: &na::DMatrix<i16>) -> na::DMatrix<u16> {
    grad_x.zip_map(grad_y, |gx, gy| {
        let gx = i32::from(gx);
        let gy = i32::from(gy);
        (gx * gx + gy * gy) as u16
    })
}

#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::cast_sign_loss)]
pub fn squared_norm_direct(img: &na::DMatrix<u8>) -> na::DMatrix<u16> {
    let (nb_rows, nb_cols) = img.shape();
    let mut squared_norm_mat = na::DMatrix::zeros(nb_rows, nb_cols);
    for j in 1..nb_cols - 2 {
        for i in 1..nb_rows - 2 {
            let gx = i32::from(img[(i + 1, j)]) - i32::from(img[(i - 1, j)]);
            let gy = i32::from(img[(i, j + 1)]) - i32::from(img[(i, j - 1)]);
            squared_norm_mat[(i, j)] = ((gx * gx + gy * gy) / 4) as u16;
        }
    }
    squared_norm_mat
}

// BLOCS 2x2 ###################################################################

/// Horizontal gradient in a 2x2 pixels block.
///
/// The block is of the form:
///   a c
///   b d
pub fn bloc_x(a: u8, b: u8, c: u8, d: u8) -> i16 {
    let a = i16::from(a);
    let b = i16::from(b);
    let c = i16::from(c);
    let d = i16::from(d);
    (c + d - a - b) / 2
}

/// Vertical gradient in a 2x2 pixels block.
///
/// The block is of the form:
///   a c
///   b d
pub fn bloc_y(a: u8, b: u8, c: u8, d: u8) -> i16 {
    let a = i16::from(a);
    let b = i16::from(b);
    let c = i16::from(c);
    let d = i16::from(d);
    (b - a + d - c) / 2
}

/// Gradient squared norm in a 2x2 pixels block.
///
/// The block is of the form:
///   a c
///   b d
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::cast_sign_loss)]
pub fn bloc_squared_norm(a: u8, b: u8, c: u8, d: u8) -> u16 {
    let a = i32::from(a);
    let b = i32::from(b);
    let c = i32::from(c);
    let d = i32::from(d);
    let dx = c + d - a - b;
    let dy = b - a + d - c;
    ((dx * dx + dy * dy) / 4) as u16
}


#[cfg(test)]
mod tests {

    use super::*;
    use na::{DMatrix};

    #[test]
    fn gradient() {
        let src = DMatrix::from_row_slice(
            2,
            2,
            &[0 as u8, 1 as u8, 1 as u8, 0 as u8]
        );
        let (grad_x, grad_y) = centered(&src);
        assert!(grad_x.eq(
            &DMatrix::from_row_slice(
            2,
            2,
            &[0 as i16, 1 as i16, 1 as i16, 0 as i16]
        )));
        assert!(grad_y.eq(
            &DMatrix::from_row_slice(
            2,
            2,
            &[0 as i16, 1 as i16, 1 as i16, 0 as i16]
        )));
    }

}

