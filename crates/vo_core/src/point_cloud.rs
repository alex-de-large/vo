use crate::types::Point3;

pub struct PointCloud {
    pub data: Vec<Point3>,
}

impl PointCloud {

    pub fn new() -> Self {
        Self { data: vec![] }
    }

    pub fn append(&mut self, points: &mut Vec<Point3>, colors: &mut Vec<Point3>) {
        // self.data.append(other);
        assert_eq!(points.len(), colors.len());
        for (p, c) in points.drain(..).zip(colors.drain(..)) {
            self.data.push(p);
            self.data.push(c);
        }
    }

    pub fn push(&mut self, point: Point3, color: Point3) {
        self.data.push(point);
        self.data.push(color);
    }
}
