use crate::types::Float;

/// An inverse depth can be one of three values: unknown, discarded, or known with a given
/// variance.
#[derive(Copy, Clone, PartialEq, Debug)]
pub enum InverseDepth {
    /// Not known by the capture device.
    Unknown,
    /// Value was considered too unreliable and discarded.
    Discarded,
    /// `WithVariance(inverse_depth, variance)`: known but with a given uncertainty.
    WithVariance(Float, Float),
}

/// Transform a depth value from a depth map into an inverse depth value with a given scaling.
///
/// A value of 0 means that it is unknown.
pub fn from_depth(scale: Float, depth: u16, variance: Float) -> InverseDepth {
    match depth {
        0 => InverseDepth::Unknown,
        _ => InverseDepth::WithVariance(scale / Float::from(depth), variance),
    }
}

/// Transform inverse depth value back into a depth value with a given scaling.
///
/// Unknown or discarded values are encoded with 0.
#[allow(clippy::cast_precision_loss)]
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::cast_sign_loss)]
pub fn to_depth(scale: Float, idepth: InverseDepth) -> u16 {
    match idepth {
        InverseDepth::WithVariance(x, _) => (scale / x).round() as u16,
        _ => 0,
    }
}

// Merging strategies ######################################

/// Fuse 4 inverse depth pixels of a bloc with a given merging strategy.
///
/// It only keeps the known values and passes them as a `Vec` into the given strategy.
pub fn fuse<F>(
    a: InverseDepth,
    b: InverseDepth,
    c: InverseDepth,
    d: InverseDepth,
    strategy: F,
) -> InverseDepth
    where
        F: Fn(&[(Float, Float)]) -> InverseDepth,
{
    strategy(
        [a, b, c, d]
            .iter()
            .filter_map(with_variance)
            .collect::<Vec<_>>()
            .as_slice(),
    )
}

fn with_variance(idepth: &InverseDepth) -> Option<(Float, Float)> {
    if let InverseDepth::WithVariance(idepth, var) = *idepth {
        Some((idepth, var))
    } else {
        None
    }
}

/// Merge idepth pixels of a bloc into their mean idepth.
pub fn strategy_mean(valid_values: &[(Float, Float)]) -> InverseDepth {
    match valid_values {
        [(d1, v1)] => InverseDepth::WithVariance(*d1, *v1),
        [(d1, v1), (d2, v2)] => {
            let sum = v1 + v2;
            InverseDepth::WithVariance((d1 * v1 + d2 * v2) / sum, sum)
        }
        [(d1, v1), (d2, v2), (d3, v3)] => {
            let sum = v1 + v2 + v3;
            InverseDepth::WithVariance((d1 * v1 + d2 * v2 + d3 * v3) / sum, sum)
        }
        [(d1, v1), (d2, v2), (d3, v3), (d4, v4)] => {
            let sum = v1 + v2 + v3 + v4;
            InverseDepth::WithVariance((d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4) / sum, sum)
        }
        _ => InverseDepth::Unknown,
    }
}
